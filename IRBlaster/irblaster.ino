#include <IRremote.h>

#define optOFF 0x4CB3748B
#define optON 0x4CB340BF

IRsend irsend;
int bitLen = 32;

int turnProjectorOff(String command);
int turnProjectorOff(String command);

void setup()
{
    Particle.function("turnProjectorOn", turnProjectorOn);
    Particle.function("turnProjectorOff", turnProjectorOff);
}

void loop() {

}

int turnProjectorOn(String command) {
    Particle.publish("Turning on projector");
    irsend.sendNEC(optON, bitLen);
    delay(1000);
    
    return 1;
}

int turnProjectorOff(String command) {
    Particle.publish("Turning off projector");
    irsend.sendNEC(optOFF, bitLen);
    delay(2000);
    irsend.sendNEC(optOFF, bitLen);
    delay(1000);
    
    return 1;
}